﻿namespace PotterBooks.Core
{
    public class SeriesDiscount
    {
        public BookSeries Series { get; set; }
        public int Count { get; set; }
        public double DiscountPercentage { get; set; }
    }
}
