﻿using System.Collections.Generic;

namespace PotterBooks.Core
{
    public class BookSeries
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public List<Book> Books { get; } = new List<Book>();
    }
}