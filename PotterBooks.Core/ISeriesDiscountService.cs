﻿using System.Collections.Generic;

namespace PotterBooks.Core
{
    public interface ISeriesDiscountService
    {
        IDictionary<int, SeriesDiscount> GetDiscounts();
    }
}