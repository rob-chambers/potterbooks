﻿namespace PotterBooks.Core
{
    public static class Stock
    {
        static Stock()
        {
            HarryPotter = new BookSeries
            {
                Id = 1,
                Title = "Harry Potter"
            };

            AddTitles();
            SetSeriesOnTitles();
        }

        private static void AddTitles()
        {
            HarryPotter.Books.Add(SorcerersStone);
            HarryPotter.Books.Add(ChamberOfSecrets);
            HarryPotter.Books.Add(OrderOfPhoenix);
            HarryPotter.Books.Add(GobletOfFire);
            HarryPotter.Books.Add(PrisonerOfAzkaban);
        }

        private static void SetSeriesOnTitles()
        {
            foreach (var book in HarryPotter.Books)
            {
                book.Series = HarryPotter;
            }
        }

        public static BookSeries HarryPotter { get; private set; }

        public static Book SorcerersStone = new Book
        {
            Id = 101,
            Title = "Harry Potter and the Sorcerer's Stone",
            Price = Book.SeriesPrice,
            Series = HarryPotter
        };

        public static Book ChamberOfSecrets = new Book
        {
            Id = 102,
            Title = "Harry Potter and the Chamber of Secrets",
            Price = Book.SeriesPrice,
            Series = HarryPotter
        };

        public static Book OrderOfPhoenix = new Book
        {
            Id = 103,
            Title = "Harry Potter and the Order of the Phoenix",
            Price = Book.SeriesPrice,
            Series = HarryPotter
        };

        public static Book GobletOfFire = new Book
        {
            Id = 104,
            Title = "Harry Potter and the Goblet of Fire",
            Price = Book.SeriesPrice,
            Series = HarryPotter
        };

        public static Book PrisonerOfAzkaban = new Book
        {
            Id = 105,
            Title = "Harry Potter and the Prisoner of Azkaban",
            Price = Book.SeriesPrice,
            Series = HarryPotter
        };
    }
}
