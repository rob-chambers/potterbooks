﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PotterBooks.Core
{
    public class BasketPriceCalculator
    {
        private readonly ISeriesDiscountService _seriesDiscountService;

        public BasketPriceCalculator(ISeriesDiscountService seriesDiscountService)
        {
            _seriesDiscountService = seriesDiscountService;
        }

        public decimal CalculatePrice(Basket basket)
        {
            decimal total = 0;
            var count = 0;

            var setGroups = from book in basket
                            group book by book.Id into setGroup
                            orderby setGroup.Key
                            select new
                            {
                                BookId = setGroup.Key,
                                Count = setGroup.Count()
                            };

            if (!setGroups.Any())
            {
                return 0;
            }

            var pile = new Dictionary<int, int>();

            // Set up piles
            var booksSeries = basket
                .Where(b => b.Series != null)
                .Select(b => b.Series)
                .Distinct();

            if (!booksSeries.Any())
            {
                return basket.Sum(x => x.Price);
            }

            foreach (var series in booksSeries)
            {
                foreach (var bookId in series.Books.Select(x => x.Id))
                {
                    pile.Add(bookId, 0);
                };
            }

            // Add basket to piles
            var bookCount = 0;
            foreach (var setGroup in setGroups)
            {
                pile[setGroup.BookId] = setGroup.Count;
                bookCount += setGroup.Count;
            }

            var counts = new Dictionary<int, int>();

            while (bookCount > 0)
            {
                var ids = new List<int>();
                foreach (var bookId in pile.Keys)
                {
                    if (pile[bookId] > 0)
                    {
                        // take a book off the pile
                        ids.Add(bookId);
                    }
                }

                // Take one off each pile
                count = 0;
                foreach (var id in ids)
                {
                    count++;
                    pile[id]--;
                    bookCount--;
                }

                if (count > 0)
                {
                    if (counts.ContainsKey(count))
                    {
                        counts[count]++;
                    }
                    else
                    {
                        counts.Add(count, 1);
                    }
                }
            }

            OptimiseForBiggestDiscount(counts);

            var discounts = _seriesDiscountService.GetDiscounts();
            foreach (var setCount in counts.Keys)
            {
                var numberOfBooks = setCount * counts[setCount];
                var percentageDiscount = 1 - Convert.ToDecimal(discounts[setCount].DiscountPercentage) / 100;
                total += Book.SeriesPrice * numberOfBooks * percentageDiscount;
            }

            return total;
        }

        private static void OptimiseForBiggestDiscount(Dictionary<int, int> counts)
        {
            if (!counts.ContainsKey(5) || !counts.ContainsKey(3))
            {
                return;
            }

            if (counts.ContainsKey(4))
            {
                counts[4] += 2;
            }
            else
            {
                counts.Add(4, 2);
            }

            // Check for multiple 5's
            counts[5]--;
            if (counts[5] == 0)
            {
                counts.Remove(5);
            }

            counts[3]--;
            if (counts[3] == 0)
            {
                counts.Remove(3);
            }
        }
    }
}