using PotterBooks.Core;
using Xunit;

namespace PotterBooks.Tests
{
    public class BasketPriceCalculatorTests
    {
        [Fact]
        public void EmptyBasketIsFree()
        {
            var calculator = new BasketPriceCalculator(new SeriesDiscountService());
            var basket = new Basket();

            var price = calculator.CalculatePrice(basket);

            Assert.Equal(0, price);
        }

        [Fact]
        public void SingleItemGetsNoDiscount()
        {
            var calculator = new BasketPriceCalculator(new SeriesDiscountService());
            var basket = new Basket
            {
                Stock.ChamberOfSecrets
            };

            var price = calculator.CalculatePrice(basket);

            Assert.Equal(8, price);
        }

        [Fact]
        public void TwoBooksOfSameTitleGetsZeroDiscount()
        {
            var calculator = new BasketPriceCalculator(new SeriesDiscountService());
            var basket = new Basket
            {
                { Stock.ChamberOfSecrets, 2 }
            };

            var price = calculator.CalculatePrice(basket);

            Assert.Equal(8 * 2, price);
        }

        [Fact]
        public void TwoBooksInSeriesGetsSmallDiscount()
        {
            var calculator = new BasketPriceCalculator(new SeriesDiscountService());
            var basket = new Basket
            {
                Stock.ChamberOfSecrets,
                Stock.GobletOfFire
            };

            var price = calculator.CalculatePrice(basket);

            Assert.Equal(8 * 2 * 0.95M, price);
        }

        [Fact]
        public void ThreeBooksInSeriesGetsTenPercentDiscount()
        {
            var calculator = new BasketPriceCalculator(new SeriesDiscountService());
            var basket = new Basket
            {
                Stock.ChamberOfSecrets,
                Stock.GobletOfFire,
                Stock.OrderOfPhoenix
            };

            var price = calculator.CalculatePrice(basket);

            Assert.Equal(8 * 3 * 0.9M, price);
        }

        [Fact]
        public void FourBooksInSeriesGetsTwentyPercentDiscount()
        {
            var calculator = new BasketPriceCalculator(new SeriesDiscountService());
            var basket = new Basket
            {
                Stock.ChamberOfSecrets,
                Stock.GobletOfFire,
                Stock.OrderOfPhoenix,
                Stock.PrisonerOfAzkaban
            };

            var price = calculator.CalculatePrice(basket);

            Assert.Equal(8 * 4 * 0.8M, price);
        }

        [Fact]
        public void FiveBooksInSeriesGetsTwentyFivePercentDiscount()
        {
            var calculator = new BasketPriceCalculator(new SeriesDiscountService());
            var basket = new Basket
            {
                Stock.ChamberOfSecrets,
                Stock.GobletOfFire,
                Stock.OrderOfPhoenix,
                Stock.PrisonerOfAzkaban,
                Stock.SorcerersStone
            };

            var price = calculator.CalculatePrice(basket);

            Assert.Equal(8 * 5 * 0.75M, price);
        }

        [Fact]
        public void ComplicatedBasketCalculatedCorrectly()
        {
            var calculator = new BasketPriceCalculator(new SeriesDiscountService());
            var basket = new Basket
            {
                { Stock.ChamberOfSecrets, 2 },
                { Stock.GobletOfFire, 2 },
                { Stock.OrderOfPhoenix, 2 },
                Stock.PrisonerOfAzkaban,
                Stock.SorcerersStone
            };

            var price = calculator.CalculatePrice(basket);

            Assert.Equal(2 * 4 * 8 * 0.8M, price);
        }

        [Fact]
        public void BigBasketCalculatedCorrectly()
        {
            var calculator = new BasketPriceCalculator(new SeriesDiscountService());
            var basket = new Basket
            {
                { Stock.ChamberOfSecrets, 10 },
                { Stock.GobletOfFire, 3 },
                Stock.OrderOfPhoenix
            };

            var price = calculator.CalculatePrice(basket);

            Assert.Equal(108M, price);
        }

        [Fact]
        public void FiveAndTwoSetBasketCalculatedCorrectly()
        {
            var calculator = new BasketPriceCalculator(new SeriesDiscountService());
            var basket = new Basket
            {
                { Stock.ChamberOfSecrets, 2 },
                { Stock.GobletOfFire, 2 },
                Stock.OrderOfPhoenix,
                Stock.PrisonerOfAzkaban,
                Stock.SorcerersStone
            };

            var price = calculator.CalculatePrice(basket);

            // 5 * 8 * 0.75 = 30
            // 2 * 8 * 0.95 = 15.20
            Assert.Equal(45.20M, price);
        }

        [Fact]
        public void FourAndOneSetBasketCalculatedCorrectly()
        {
            var calculator = new BasketPriceCalculator(new SeriesDiscountService());
            var basket = new Basket
            {
                { Stock.ChamberOfSecrets, 2 },
                Stock.GobletOfFire,
                Stock.OrderOfPhoenix,
                Stock.PrisonerOfAzkaban
            };

            var price = calculator.CalculatePrice(basket);

            Assert.Equal(33.6M, price);
        }

        [Fact]
        public void NonHarryPotterBooks()
        {
            const decimal Price1 = 10M;
            const decimal Price2 = 15M;

            var calculator = new BasketPriceCalculator(new SeriesDiscountService());
            var basket = new Basket
            {
                new Book
                {
                    Id = 100,
                    Price = Price1,
                    Title = "C# Made Easy"
                },
                new Book
                {
                    Id = 101,
                    Price = Price2,
                    Title = "Programming for dummies"
                }
            };

            var price = calculator.CalculatePrice(basket);

            var expected = Price1 + Price2;
            Assert.Equal(expected, price);
        }
    }
}
