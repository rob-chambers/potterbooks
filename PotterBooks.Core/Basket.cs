﻿using System.Collections.Generic;
using System.Linq;

namespace PotterBooks.Core
{
    public class Basket : Stack<Book>
    {
        public bool IsEmpty => !this.Any();

        public void Add(Book book, int count = 1)
        {
            for (var index = 0; index < count; index++)
            {
                Push(book);
            }
        }
    }
}
