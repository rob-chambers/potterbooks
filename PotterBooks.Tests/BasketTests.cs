﻿using PotterBooks.Core;
using Xunit;

namespace PotterBooks.Tests
{
    public class BasketTests
    {
        [Fact]
        public void BasketInitiallyEmpty()
        {
            var basket = new Basket();
            Assert.True(basket.IsEmpty);
        }

        [Fact]
        public void AddingOneItemCorrect()
        {
            var basket = new Basket
            {
                new Book()
            };
            
            Assert.Single(basket);
        }

        [Fact]
        public void AddingTwoItemsCorrect()
        {
            var basket = new Basket
            {
                { new Book(), 2 }
            };

            Assert.Equal(2, basket.Count);
        }
    }
}
