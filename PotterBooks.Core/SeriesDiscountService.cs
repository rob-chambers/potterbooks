﻿using System.Collections.Generic;

namespace PotterBooks.Core
{
    public class SeriesDiscountService : ISeriesDiscountService
    {
        public IDictionary<int, SeriesDiscount> GetDiscounts()
        {
            var discounts = new Dictionary<int, SeriesDiscount>
            {
                // Assume we have a discount for every single title in the series
                {
                    1,
                    new SeriesDiscount
                    {
                        Count = 1,
                        DiscountPercentage = 0,
                        Series = Stock.HarryPotter
                    }
                },
                {
                    2,
                    new SeriesDiscount
                    {
                        Count = 2,
                        DiscountPercentage = 5,
                        Series = Stock.HarryPotter
                    }
                },
                {
                    3,
                    new SeriesDiscount
                    {
                        Count = 3,
                        DiscountPercentage = 10,
                        Series = Stock.HarryPotter
                    }
                },
                {
                    4,
                    new SeriesDiscount
                    {
                        Count = 4,
                        DiscountPercentage = 20,
                        Series = Stock.HarryPotter
                    }
                },
                {
                    5,
                    new SeriesDiscount
                    {
                        Count = 5,
                        DiscountPercentage = 25,
                        Series = Stock.HarryPotter
                    }
                }
            };

            return discounts;
        }
    }
}