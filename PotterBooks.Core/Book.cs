﻿namespace PotterBooks.Core
{
    public class Book
    {
        public const decimal SeriesPrice = 8;

        public int Id { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
        public BookSeries Series { get; set; }
    }
}